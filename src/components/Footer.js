import React, { Fragment } from "react";
import { Container } from "react-bootstrap";

export default function Footer() {
  return (
    <Fragment>
      <Container className="mr-auto">
        <h1> This is Footer</h1>
      </Container>
    </Fragment>
  );
}
