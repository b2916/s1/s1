import React from "react";

import { Jumbotron } from "react-bootstrap";

export default function aboutComponents() {
  return (
    <Jumbotron>
      <h1>About me</h1>
      <p>
        Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Rerum illum
        accusamus repellat beatae est, in quaerat earum, aliquid ducimus commodi
        eaque omnis ullam quo eos quis sapiente corrupti maxime obcaecati.
      </p>
    </Jumbotron>
  );
}
